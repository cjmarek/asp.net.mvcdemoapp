﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChrisMarekPhotography.Common;
using ChrisMarekPhotography.Models;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace ChrisMarekPhotography.Tests
{
    //Note to self. Don't test the X-Manager classes. Those are used as dependency-breakers
    //Trying to test them is weird. Its testing the testing. Only test the Domain classes.
    //X-Manager classes such as DirectoriesManager, FileReaderManager, FolderFilesManager, ServerPathManager
    //REMINDER. The Factory class is responsible for handing out object instances (to the Dao or the Facade) That is why you are
    // sending testing stubs to the Factory. It takes me a while to realize this, I keep thinking I should be initializing the Dao or Facade directly with stubs. NO!
    public class FacadeTests
    {
        private readonly IFacade _facade;
        private readonly IDao _dao;
        private const string basePath = @"\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\";


        private readonly ITestOutputHelper _output;

        /// <summary>
        /// NOTE. We get into this constructor each time a test completes. 
        /// And somehow the tests don't run in sequential order from top down.
        /// And for tests that have InLineData, the InLineData does not
        /// get used from top to bottom, or from bottom to top. Rather, the InLineData get used randomly!
        /// ITestOutputHelper is being used for debugging purposes here,
        /// otherwise get rid of it
        /// </summary>
        /// <param name="output"></param>
        public FacadeTests(ITestOutputHelper output)
        {
            _output = output;
            //TearDown();   don't need this after all.
            _dao = new Dao();//this puts a production level Dao into the factory
            _facade = new Facade();
        }
        /// <summary>
        /// My unit tests used to be two seperate tests, one for DaoTests.cs and one for FacadeTests.cs. But it failed because
        /// they seemed to somehow corrupt each other, causing a few tests to fail and I never figured out how/why. So I combined the 
        /// DaoTests.cs into the FacadeTests.cs here, and backed up the old DaoTests.cs and the FacadeTests.cs. That fixed
        /// the problem I am complaining about here.
        /// The following comment is obsolete, but I left it in.
        /// There seems to be some weird problem that causes several of the tests to fail. And it is the same tests that fail.
        /// And I thought maybe because I am using a static class for factory, somehow the data from one test was carrying over
        /// to the next test. But my TearDown should be taking care of that. WTF
        /// </summary>
        private void TearDown()
        {
            //I added this on 11/03/2024 thinking that the static class was hanging onto data between runs maybe?
            //It should NOT be necessary to initial these to null because I use a setter method on any dependencies
            //Factory._photosResult = null;
            //Factory._photograph = null;
            //Factory._photoInformation = null;
            //Factory._photoObject = null;
            //Factory._photoViewModel = null;
            //Factory._dao = null;
            //Factory._facade = null;
            //Factory._directoriesManager = null;
            //Factory._fileReaderManager = null;
            //Factory._pathManager = null;
            //Factory._folderFilesManager = null;
            //_dao = new Dao();
        }

        /// <summary>
        /// 
        /// * * * * * * * * * * * * * * START TO DAO TEST  * * * * * * * * * * * * * *
        /// 
        /// These two tests (GetAllPhotoFolders_ShouldBeEqualToExpected and GetArray_ShouldBeEqualToExpected) Used to be in
        /// a separate DaoTests.cs. But somehow, that caused the FacadeTests and the  DaoTests to corrupt each other.
        /// So I moved the DaoTests here into the FacadeTests, and now all the tests are working.
        /// </summary>
        /// <param name="directories"></param>
        /// <param name="expected"></param>


        [Theory]
        [InlineData(new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird",
            @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\cactus",
            @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\butterfly",
            @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\egret" },
            new string[] { "bluebird", "cactus", "butterfly", "egret" })]
        public void GetAllPhotoFolders_ShouldBeEqualToExpected(string[] directories, string[] expected)
        {
            //Arrange
            StubDirectoriesManager dm = new StubDirectoriesManager();
            dm.directories = directories;

            Factory.SetDirectoriesManagerDependency(dm); //this puts a stubDirectoriesManager onto the Factory (replacing DirectoriesManager)

            //Act                    
            var actual = _dao.GetAllPhotoFolders().ToArray(); //_dao is a production level Dao, it will be using the StubDirectoriesManager from the Factory! 

            string[] result = actual.Where(act => expected.Search(exp => exp == act)).ToArray();

            //Assert
            Assert.Equal(4, result.Count());
            for (int i = 0; i < 4; i++)
            {
                Assert.Equal(expected[i], actual[i]);
            }

            //TearDown();
        }


        /// <summary>
        /// Obsolete comment, I just decided to leave it in
        /// Tests are running inconsistently. But...
        /// When I comment this test out, that fixes my problems with all the other tests! Why? See the following comments
        /// </summary>

        [Theory]
        [InlineData(@"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\egret\indexes.txt", new string[] { "1", "2", "3" }, new string[] { "foo", "boo", "whatever", "blah" }, new string[] { "1", "2", "3" })]
        [InlineData(@"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\egret\details.txt", new string[] { "1", "2", "3" }, new string[] { "foo", "boo", "whatever", "blah" }, new string[] { "foo", "boo", "whatever", "blah" })]
        public void GetArray_ShouldBeEqualToExpected(string path, string[] indexes, string[] details, string[] expected)
        {
            //Arrange
            StubFileReaderManager fm = new StubFileReaderManager();
            fm.indexes = indexes;
            fm.details = details;

            Factory.SetFileReaderManagerDependency(fm); //this puts a StubFileReaderManager onto the Factory (replacing FileReaderManager)

            //Act
            var actual = _dao.GetArray(path);  //_dao is a production level Dao, it will be using the StubFileReaderManager from the Factory!

            //note to self, don't try to compare equal against details == expected or indexes == expected because these are objects and diff objects
            //will not compare. see tim corey unit test video
            //Assert
            Assert.Equal(expected.Length, actual.Length);

            //TearDown();
        }
        /// <summary>
        /// This version of StubFileReaderManager() has no logic in it. I think maybe that is the way it should be to keep the test from becoming fragile.
        /// Also, there was a odd problem I was having with my tests, that I could not figure out. But the problem went away when I commented out the test above. Somehow it must be
        /// because the StubFileReaderManager() above has logic in it.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="indexes"></param>
        /// <param name="expected"></param>
        [Theory]
        [InlineData(@"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\egret\indexes.txt", new string[] { "1", "2", "3" }, new string[] { "1", "2", "3" })]
        public void GetArray_indexesShouldBeEqualToExpected2(string path, string[] indexes, string[] expected)
        {
            //Arrange
            StubFileReaderManager2 fm = new StubFileReaderManager2();
            fm.data = indexes;

            Factory.SetFileReaderManagerDependency(fm); //this puts a StubFileReaderManager onto the Factory (replacing FileReaderManager)

            //Act                   there is no need to pass in a path to _dao.GetArray since I removed logic from the stub to distinguish a path for indexes.txt from a path for details.txt
            var actual = _dao.GetArray(path);  //_dao is a production level Dao, it will be using the StubFileReaderManager from the Factory!

            //note to self, don't try to compare equal against details == expected or indexes == expected because these are objects and diff objects
            //will not compare. see tim corey unit test video
            //Assert
            Assert.Equal(expected.Length, actual.Length);

            //TearDown();
        }

        [Theory]
        [InlineData(@"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\egret\details.txt", new string[] { "foo", "boo", "whatever", "blah" }, new string[] { "foo", "boo", "whatever", "blah" })]
        public void GetArray_detailsShouldBeEqualToExpected3(string path, string[] details, string[] expected)
        {
            //Arrange
            StubFileReaderManager2 fm = new StubFileReaderManager2();
            fm.data = details;

            Factory.SetFileReaderManagerDependency(fm); //this puts a StubFileReaderManager onto the Factory (replacing FileReaderManager)

            //Act                   there is no need to pass in a path to _dao.GetArray since I removed logic from the stub to distinguish a path for indexes.txt from a path for details.txt
            var actual = _dao.GetArray(path);  //_dao is a production level Dao, it will be using the StubFileReaderManager from the Factory!

            //note to self, don't try to compare equal against details == expected or indexes == expected because these are objects and diff objects
            //will not compare. see tim corey unit test video
            //Assert
            Assert.Equal(expected.Length, actual.Length);

            //TearDown();
        }
        /// * * * * * * * * * * * * * * END TO DAO TEST  * * * * * * * * * * * * * *


        /// <summary>
        /// The method should succeed when a unique item is attemped to be added to a dictionary
        /// </summary>
        [Fact]
        public void AddPhotoToDictionary_UniqueShouldWork()
        {
            _output.WriteLine($"This is_output from AddPhotoToDictionary_UniqueShouldWork");
            Photograph photograph = new Photograph { folder = "bluebird", index = "01a" };
            Dictionary<string, IPhotograph> photographDictionary = new Dictionary<string, IPhotograph>();

            _facade.AddPhotoToDictionary(photograph, photographDictionary);
            Assert.True(photographDictionary.Count == 1);
            Assert.True(photographDictionary.ContainsKey("01a"));
            //TearDown();
        }

        /// <summary>
        /// The method should fail when a second, non-unique item is attemped to be added to a dictionary
        /// </summary>
        [Fact]
        public void AddPhotoToDictionary_NonUniqueShouldFail()
        {
            _output.WriteLine($"This is_output from AddPhotoToDictionary_NonUniqueShouldFail");
            //Arrange
            Photograph photograph = new Photograph { folder = "bluebird", index = "01a" };
            Dictionary<string, IPhotograph> photographDictionary = new Dictionary<string, IPhotograph>();
            //Act (add the same photograph twice)
            string resultMessage = _facade.AddPhotoToDictionary(photograph, photographDictionary);
            resultMessage = _facade.AddPhotoToDictionary(photograph, photographDictionary);
            //Assert
            Assert.True(resultMessage.IndexOf("already exists in folder") > -1);
            //TearDown();
        }
        /// <summary>
        /// This method should be able to return the correct data for the file path specified.
        /// For example, 
        ///      a ~indexes.txt path should retrieve data from the indexes.txt
        ///      a ~details.txt path should retrieve data from the details.txt
        /// This method is not validating the data, just retrieving it.
        /// </summary>
        /// <param name="indexes">CSV data for a indexes.txt file</param>
        /// <param name="details">CSV data for a details.txt file</param>
        /// <param name="folder">folder name</param>
        /// <param name="expected">CSV data from indexes.txt</param>
        [Theory]
        [InlineData(new string[] { "1", "2" }, new string[] { "foo", "boo", "whatever", "blah" }, @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\indexes.txt", new string[] { "1", "2" })]
        [InlineData(new string[] { "1", "2", "5" }, new string[] { "foo" }, @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\details.txt", new string[] { "foo" })]
        public void GetPhotoFolderData_RetrievesDataFromIndexesOrDetailsFile(string[] indexes, string[] details, string path, string[] expected)
        {
            //TearDown();
            //Arrange
            StubFileReaderManager fm = new StubFileReaderManager(); //FileReader is found in the Dao
            fm.indexes = indexes;
            fm.details = details;

            Factory.SetFileReaderManagerDependency(fm); //put stub into Factory to replace production code
                                                        // so now where ever the Dao calls the Factory it uses the StubFileReaderManager

            //Act
            string[] result = _facade.GetPhotoFolderData(path);


            //Assert
            Assert.Equal(expected.Length, result.Length);
            //TearDown();
        }

        /// <summary>
        /// there is a expectation of what the path is for any folder
        /// </summary>
        /// <param name="folder">folder name</param>
        /// <param name="dataFile">CSV file indexes.txt or details.txt</param>
        /// <param name="serverPath">physical path for a folder</param>
        /// <param name="expected">correct format that should be returned</param>
        [Theory]
        [InlineData("acornwoodpecker", "indexes", @"\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\photos\acornwoodpecker\indexes.txt")]
        public void GetPath_ShouldBeEqualToExpected(string folder, string dataFile, string expected)
        {
            _output.WriteLine($"This is_output from GetPath_ShouldBeEqualToExpected");
            //Arrange
            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = basePath;
            Factory.SetServerPathManagerDependency(spm);

            //Act
            string result = _facade.GetPath(folder, dataFile);
            //Assert
            Assert.True(expected == result);
            //TearDown();
        }

        /// <summary>
        /// The GetAllPhotographIndexes is never supposed to retrieve indexes from the slideshow and portfolio folders.
        /// </summary>
        /// <param name="indexes"></param>
        /// <param name="directories"></param>
        /// <param name="serverPath"></param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3", "4", "5", "6" }, new string[] { "acornwoodpecker" }, 6)]
        [InlineData(new string[] { "1", "2", "3" }, new string[] { "slideshow" }, 0)]
        [InlineData(new string[] { "1", "2", "3", "4" }, new string[] { "portfolio" }, 0)]
        public void GetAllPhotographIndexes_PortfolioSlideshowCountAlwaysZero(string[] indexes, string[] directories, int expected)
        {
            _output.WriteLine($"This is_output from GetAllPhotographIndexes_PortfolioSlideshowCountAlwaysZero");
            //Arrange
            StubFileReaderManager fm = new StubFileReaderManager();
            fm.indexes = indexes;

            StubDirectoriesManager dm = new StubDirectoriesManager();
            dm.directories = directories;

            StubFileManager sfm = new StubFileManager();
            sfm.files = new string[] { "indexes.txt" }; //slideshow or portfolio only have a indexes.txt file in their folder

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = Directory.GetCurrentDirectory().Split('\\')[0] + basePath;

            Factory.SetFolderFilesManagerDependency(sfm); //initializes DaoDependency with stub

            Factory.SetFileReaderManagerDependency(fm);   //initializes DaoDependency with stub

            Factory.SetDirectoriesManagerDependency(dm);  //initializes DaoDependency with stub

            Factory.SetServerPathManagerDependency(spm);  //initializes FacadeDependency with stub

            //Act
            int count = _facade.GetAllPhotographIndexes().Count;

            for (int i = 0; i < indexes.Length; i++)
            {
                _output.WriteLine($" {indexes[i]}");
            }

            _output.WriteLine($"This is_output from GetAllPhotographIndexes_PortfolioSlideshowCountAlwaysZero count {count}, expected {expected}");

            //Assert
            Assert.True(expected == count);
            //TearDown();
        }

        /// <summary>
        /// The GetAllPhotographIndexes is always supposed to retrieve indexes from the VALIDATED
        /// folders with the exception of slideshow and portfolio folders.
        /// Note, test 2 and test three don't use a details array. portfolio and slide show only need a list of photo indexes.
        /// Note, test 4 has expected count zero because the details for this has only 2 entries and details is supposed to always have a minimum of 4 entries.
        /// So test four is testing the validation logic in the application domain. So this is an invalid folder, which can not return a list of indexes.
        /// </summary>
        /// <param name="indexes"></param>
        /// <param name="directories"></param>
        /// <param name="serverPath"></param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3" }, new string[] { "foo", "boo", "whatever", "blah" }, new string[] { "acornwoodpecker" }, 3)]
        [InlineData(new string[] { "1", "2", "3", "4", "5" }, new string[] { null }, new string[] { "portfolio" }, 0)]
        [InlineData(new string[] { "1", "2", "3", "4", "5" }, new string[] { null }, new string[] { "slideshow" }, 0)]
        [InlineData(new string[] { "1", "2", "3", "4", "5" }, new string[] { "1", "2" }, new string[] { "bluebird" }, 0)]
        [InlineData(new string[] { "1", "2", "3", "4", "5" }, new string[] { "foo", "boo", "whatever", "blah" }, new string[] { "bluebird" }, 5)]
        public void GetAllPhotographIndexes_ValidfoldersAlwaysReturnIndexes(string[] indexes, string[] details, string[] directories, int expected)
        {
            //Arrange
            StubFileReaderManager fm = new StubFileReaderManager();
            fm.indexes = indexes;
            fm.details = details;

            StubDirectoriesManager dm = new StubDirectoriesManager();
            dm.directories = directories;

            StubFileManager sfm = new StubFileManager();
            sfm.files = new string[] { "indexes.txt", "details.txt" };

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = Directory.GetCurrentDirectory().Split('\\')[0] + basePath;

            Factory.SetFolderFilesManagerDependency(sfm); //initializes DaoDependency with stub
            Factory.SetFileReaderManagerDependency(fm);   //initializes DaoDependency with stub
            Factory.SetDirectoriesManagerDependency(dm);  //initializes DaoDependency with stub
            Factory.SetServerPathManagerDependency(spm);  //initializes FacadeDependency with stub

            //Act
            int count = _facade.GetAllPhotographIndexes().Count;

            //Assert
            Assert.Equal(expected, count);
            //TearDown();
        }
        /// <summary>
        /// indexes.txt must have greater than 0 parts or it will fail validation and the folder is declared invalid
        /// </summary>
        /// <param name="indexes">CSV data from a indexes.txt file</param>
        /// <param name="directories">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="serverPath">the path of the directory</param>
        /// <param name="fileNames">the files inside a folder</param>
        [Theory]
        [InlineData(new string[] { "1" }, new string[] { "foo", "boo", "whatever", "blah" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\acornwoodpecker", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\redwingedblackbird" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\indexes.txt" })]
        [InlineData(new string[] { "1", "2", "3", "4" }, new string[] { "foo", "boo", "whatever", "blah" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\acornwoodpecker", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\redwingedblackbird" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\indexes.txt" })]
        public void GetAllPhotographIndexes_ValidIndexesWhenGreaterThanZero(string[] indexes, string[] details, string[] directories, string[] fileNames)
        {
            _output.WriteLine($"This is_output from GetAllPhotographIndexes_ValidIndexesWhenGreaterThanZero");
            //Arrange
            StubFileReaderManager sfrm = new StubFileReaderManager();
            sfrm.indexes = indexes;
            sfrm.details = details;

            StubDirectoriesManager sdm = new StubDirectoriesManager();
            sdm.directories = directories;

            StubFileManager sfm = new StubFileManager();
            sfm.files = fileNames;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = Directory.GetCurrentDirectory().Split('\\')[0] + basePath;

            Factory.SetDirectoriesManagerDependency(sdm);  //initializes DaoDependency with stub
            Factory.SetFileReaderManagerDependency(sfrm);  //initializes DaoDependency with stub
            Factory.SetFolderFilesManagerDependency(sfm);  //initializes DaoDependency with stub
            Factory.SetServerPathManagerDependency(spm);   //initializes FacadeDependency with stub

            //Act
            int count = _facade.GetAllPhotographIndexes().Count;

            //Assert
            Assert.True(count > 0);
            //TearDown();
        }

        /// <summary>
        /// Details.txt must have at LEAST 4 parts or it will fail validation and NO indexs are returned because the 
        /// folder that contains that invalid Details.txt is declared invalid and not used by the application
        /// Note, test 1 has expected count zero because the details for this has only 3 entries and details is supposed to always have a minimum of 4 entries
        /// </summary>
        /// <param name="indexes">CSV data from a indexes.txt file</param>
        /// <param name="directories">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="serverPath">the path of the directory</param>
        /// <param name="fileNames">the files inside a folder</param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3", "4", "5", "6" }, new string[] { "foo", "boo", "whatever" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\acornwoodpecker", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\redwingedblackbird" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\details.txt" }, 0)]
        [InlineData(new string[] { "1", "2", "3", "4", "5" }, new string[] { "foo", "boo", "whatever", "blah" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\acornwoodpecker", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\redwingedblackbird" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\details.txt" }, 15)]
        public void ValidateFolders_DetailsFileLessThanFourPartsMustFail(string[] indexes, string[] details, string[] directories, string[] fileNames, int expected)
        {
            _output.WriteLine($"This is_output from ValidateFolders_DetailsFileLessThanFourPartsMustFail");
            //Arrange
            StubFileReaderManager sfrm = new StubFileReaderManager();
            sfrm.indexes = indexes;
            sfrm.details = details;

            StubDirectoriesManager sdm = new StubDirectoriesManager();
            sdm.directories = directories;

            StubFileManager sfm = new StubFileManager();
            sfm.files = fileNames;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = Directory.GetCurrentDirectory().Split('\\')[0] + basePath;

            Factory.SetDirectoriesManagerDependency(sdm); //initializes DaoDependency with stub
            Factory.SetFileReaderManagerDependency(sfrm); //initializes DaoDependency with stub
            Factory.SetFolderFilesManagerDependency(sfm); //initializes DaoDependency with stub
            Factory.SetServerPathManagerDependency(spm);  //initializes FacadeoDependency with stub

            //Act
            int count = _facade.GetAllPhotographIndexes().Count;

            //Assert
            Assert.True(expected == count);
            //TearDown();
        }

        /// <summary>
        ///  CSV data file can only be Indexes.txt or details.txt
        /// </summary>
        /// <param name="indexes"></param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="serverPath">the path of the directory</param>
        /// <param name="fileName">the files inside a folder</param>
        [Theory]
        [InlineData(new string[] { "Here we go gatherring nuts in may" }, new string[] { "foo", "boo", "whatever", "blah" }, @"bluebird", @"mispelleddetails")]
        public void ValidateFolders_OnlyIndexOrDetailsFileOtherWiseInvalid(string[] indexes, string[] details, string folder, string fileName)
        {
            _output.WriteLine($"This is_output from ValidateFolders_OnlyIndexOrDetailsFileOtherWiseInvalid");
            //Arrange
            StubFileReaderManager sfrm = new StubFileReaderManager();
            sfrm.indexes = indexes;
            sfrm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = Directory.GetCurrentDirectory().Split('\\')[0] + basePath;

            Factory.SetFileReaderManagerDependency(sfrm); //initializes DaoDependency with stub
            Factory.SetServerPathManagerDependency(spm); //initializes FacadeoDependency with stub


            //Act
            IPhotosResult photosResult = _facade.ValidatePhotosFolder(folder, fileName);

            //Assert
            Assert.True(photosResult.HasErrors == true);
            //TearDown();
        }
        /// <summary>
        /// if no indexes are found in the Indexes.txt file, then that will be considerred as an error
        /// </summary>
        /// <param name="indexes">CSV data from a indexes.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { }, new string[] { "foo", "boo", "whatever", "blah" }, "bluebird", "indexes")]
        public void ValidatePhotosFolder_ZeroIndexesShouldBeInValid(string[] indexes, string[] details, string folder, string fileType)
        {
            _output.WriteLine($"This is_output from ValidatePhotosFolder_ZeroIndexesShouldBeInValid");
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.indexes = indexes;
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = Directory.GetCurrentDirectory().Split('\\')[0] + basePath;


            Factory.SetServerPathManagerDependency(spm); //initializes DaoDependency with stub
            Factory.SetFileReaderManagerDependency(sfm); //initializes DaoDependency with stub


            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);
            //Assert
            Assert.True(validationResult.HasErrors == true);
            //TearDown();
        }
        /// <summary>
        /// if the CSV data is greater than 0 in the Indexes.txt file, then that will be considerred as a valid indexes.txt file
        /// </summary>
        /// <param name="indexes">CSV data from a indexes.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3" }, new string[] { "foo", "boo", "whatever", "blah" }, "bluebird", "indexes")]
        public void ValidatePhotosFolder_IndexesShouldBeValid(string[] indexes, string[] details, string folder, string fileType)
        {
            _output.WriteLine($"This is_output from ValidatePhotosFolder_IndexesShouldBeValid");
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.indexes = indexes;
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = Directory.GetCurrentDirectory().Split('\\')[0] + basePath;

            Factory.SetServerPathManagerDependency(spm); //initializes DaoDependency with stub
            Factory.SetFileReaderManagerDependency(sfm); //initializes DaoDependency with stub

            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);
            //Assert
            Assert.True(validationResult.HasErrors == false);
            //TearDown();
        }

        /// <summary>
        /// if no CSV data are found in the details.txt file, then that will be considerred as an error
        /// </summary>
        /// <param name="details">CSV data from a indexes.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { }, "bluebird", "details")]
        public void ValidatePhotosFolder_ZeroDetailsShouldBeInValid(string[] details, string folder, string fileType)
        {
            _output.WriteLine($"This is_output from ValidatePhotosFolder_ZeroDetailsShouldBeInValid");
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = Directory.GetCurrentDirectory().Split('\\')[0] + basePath;

            Factory.SetServerPathManagerDependency(spm); //initializes DaoDependency with stub
            Factory.SetFileReaderManagerDependency(sfm); //initializes DaoDependency with stub


            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);

            //Assert
            //Assert.True(validationResult.HasErrors == true, "The details should have 4 parts");
            Assert.True(validationResult.HasErrors == true);
            //TearDown();
        }
        /// <summary>
        /// if details.txt has less than 4 CSV data, then that will be considerred as an error
        /// </summary>
        /// <param name="details">CSV data from a details.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3" }, "bluebird", "details")]
        public void ValidatePhotosFolder_DetailsShouldBeInValid(string[] details, string folder, string fileType)
        {
            _output.WriteLine($"This is_output from ValidatePhotosFolder_DetailsShouldBeInValid");
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = Directory.GetCurrentDirectory().Split('\\')[0] + basePath;

            Factory.SetServerPathManagerDependency(spm); //initializes DaoDependency with stub
            Factory.SetFileReaderManagerDependency(sfm); //initializes DaoDependency with stub

            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);

            //Assert
            Assert.True(validationResult.HasErrors == true, "The details should have at least 4 parts");
            //TearDown();
        }
        /// <summary>
        /// if details.txt has 4 CSV data (or more), then that will be considerred as a valid details.txt file
        /// </summary>
        /// <param name="details">CSV data from a details.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3", "4" }, "bluebird", "details")]
        public void ValidatePhotosFolder_DetailsShouldBeValid(string[] details, string folder, string fileType)
        {
            _output.WriteLine($"This is_output from ValidatePhotosFolder_DetailsShouldBeValid");
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = Directory.GetCurrentDirectory().Split('\\')[0] + basePath;

            Factory.SetServerPathManagerDependency(spm); //initializes DaoDependency with stub
            Factory.SetFileReaderManagerDependency(sfm); //initializes DaoDependency with stub

            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);

            //Assert
            Assert.True(validationResult.HasErrors == false, "4 parts to the details should not throw an error");
            //TearDown();
        }
        /// <summary>
        /// if csv data in indexes.txt is not numeric, the file folder containing that file is invalid
        /// </summary>
        /// <param name="details">CSV data from a details.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { "1", "2A", "3", "4" }, "bluebird", "indexes")]
        public void ValidatePhotosFolder_IndexesValuesMustBeNumeric(string[] details, string folder, string fileType)
        {
            _output.WriteLine($"This is_output from ValidatePhotosFolder_IndexesValuesMustBeNumeric");
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = Directory.GetCurrentDirectory().Split('\\')[0] + basePath;

            Factory.SetServerPathManagerDependency(spm); //initializes DaoDependency with stub
            Factory.SetFileReaderManagerDependency(sfm); //initializes DaoDependency with stub


            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);

            //Assert
            Assert.True(validationResult.HasErrors == true, "data in the indexes.txt file must be numeric");
            //TearDown();
        }
    }

    //*********************************************************************************************************************************************************************************
    //*******************************************************************           STUBS           ***********************************************************************************
    //*********************************************************************************************************************************************************************************
    //   stubs represent dependencies (dependencies that you want to create simulations of). To use these stubs, you must set up the facade with test seams so that
    //   you can replace the production dependecies with equivalent simulators. So, in the test, in the arrange, you need to prime the stub with whatever fake data you
    //   can provide as InLineData, then insert the stub into the Facade so that it is what executes as the dependency. Some of my tests have dependencies that call other
    //   dependencies, so, just as long as you can arrange InLineData for those dependecies and send those dependencies into the facade. Then everything 'should' work.

    //    I spent several days trying to figure out why I am seeing inconsistent results in my tests. Sometimes they all work for many runs, only to have one or two
    //    tests fail (and always the same few tests) and I have no idea why. I did try debugging with ITestOutputHelper output to generate console output. No luck.
    //    NOTE: these same tests I have created on the .NET Core application. All of those work EVERY SINGLE time without fail. .NET CORE does not use a Factory class
    //    to new up objects. So the .NET CORE application creates all dependencies by constructor injection. Somehow that seems to be the main difference.


    //   When you create a testing stub, I must inherit from a interface. You must implement any public methods of that interface so that the stub performs the functionality
    //   that is expected of it (no matter how you go about implementing it). So for example. We know that if a IFileReader reads from an indexes array file, it must return all
    //   the element(s) from that indexes array. And if a IFileReader reads from an details array file, it must return 4 elements from that details array. 
    //   So for this FileReader stub to work, in the arrange of the unit test, I initialize this stub with indexes and details arrays, and I added a parseName method
    //   so that the FileReader method can figure out which array to return based on the path being a indexes or details file. It doesnt matter that I have added these
    //   extra properties of the FileReader stub as long as they allow the FileReader stub to behave as it should. Or does it?
    //   I have now created a new version of StubFileReaderManager, StubFileReaderManager2 and maybe that is how it should be done.
    //
    /// <summary>
    /// This method is used to read comma separated text files (details.txt and indexes.txt)
    /// and return a string array of the comma separated data that is appropriate for the file (indexes.txt or details.txt)
    /// i.e. for a details.txt file for bluebird, it returns a string array Sialia sialis,Irving Texas,birds,blue bird
    /// i.e. for a indexes.txt file for bluebird, it returns a string array 166,10,11,12,13,14,32,162,163,164,165,252,255,256,257,258,291,259,260,261
    /// </summary>
    /// <summary>
    /// Of all 4 stubbed dependencies, this one (below) has parseName logic in it. Not so sure I should have done it this way.
    /// Because, if you add too much logic to the stub, then you make the stub brittle.
    /// 
    /// 
    /// </summary>
    public class StubFileReaderManager : IFileReaderManager
    {
        public string[] indexes { get; set; }
        public string[] details { get; set; }
        public string[] FileReader(string path)
        {
            string[] arrayResult = (parseName(path) == "indexes") ? indexes : details;
            return arrayResult;
        }
        /// <summary>
        /// Finds out what file (indexes or details) that we are on. 
        /// But this is extra logic that the IFileReaderManager interface does not account for. So is it justified?
        /// </summary>
        /// <param name="path"></param>
        /// <returns>"indexes" or "details"</returns>
        public string parseName(string path)
        {
            int lastIndex = path.LastIndexOf(@"\");
            string fileName = path.Substring(lastIndex + 1);
            fileName = fileName.Split('.')[0];
            return fileName;
        }
    }

    /// <summary>
    /// I think perhaps the stub should / could be much simpler. Like this.
    /// NOTE: I pass a parameter here, but I don't do anything with it.
    /// 
    /// This method is used to read comma separated text files (details.txt and indexes.txt)
    /// and return a string array of the comma separated data that is appropriate for the file (indexes.txt or details.txt)
    /// i.e. for a details.txt file for bluebird, it returns a string array Sialia sialis,Irving Texas,birds,blue bird
    /// i.e. for a indexes.txt file for bluebird, it returns a string array 166,10,11,12,13,14,32,162,163,164,165,252,255,256,257,258,291,259,260,261
    /// </summary>
    public class StubFileReaderManager2 : IFileReaderManager
    {
        public string[] data { get; set; }
        public string[] FileReader(string path)
        {
            string[] arrayResult = data;
            return arrayResult;
        }
    }

    /// <summary>
    /// returns string[] data file names like indexes or details 
    /// returns string[] directories of physical path names of folders from under the photos folder. 
    /// i.e. "C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\photos\\acornwoodpecker"
    /// i.e. "C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\photos\\ameicancoot"
    /// i.e. "C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\photos\\anhinga"
    /// i.e. "C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\photos\\barnswallow"
    /// i.e.   . . .and all the rest.
    /// </summary>
    public class StubDirectoriesManager : IDirectoriesManager
    {
        public string[] directories { get; set; }
        public string[] GetAllPhotoFolders()
        {
            return directories;
        }
    }

    /// <summary>
    /// returns string[] data file names like indexes or details 
    /// NOTE: I pass a parameter to this stub here, but I don't do anything with it.
    /// This class returns string arrays of the physical paths for the files in a specfic folder.
    /// i.e.C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\\\photos\\acornwoodpecker\\details.txt"
    /// i.e.C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\\\photos\\acornwoodpecker\\indexes.txt"
    /// </summary>
    public class StubFileManager : IFolderFilesManager
    {
        public string[] files { get; set; }
        public string[] GetPhotoFolderFiles(string folder)
        {
            return files;
        }
    }

    /// <summary>
    /// This method figures out the physical file path on the web server for the application.
    /// i.e. "C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\"
    /// </summary>
    public class StubServerPathManager : IPathManager
    {
        public string serverPath { get; set; }
        public string GetPath()
        {
            return serverPath;
        }
    }
}
