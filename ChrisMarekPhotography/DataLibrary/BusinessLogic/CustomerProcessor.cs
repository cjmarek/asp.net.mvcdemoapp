﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLibrary.DataAccess;
using DataLibrary.Models;

namespace DataLibrary.BusinessLogic
{
    public static class CustomerProcessor
    {

        public static int CreateCustomer(string firstName,
            string lastName, string emailAddress)
        {
            CustomerModel data = new CustomerModel
            {
                FirstName = firstName,
                LastName = lastName,
                EmailAddress = emailAddress
            };

            string sql = @"insert into dbo.Customer (FirstName, LastName, EmailAddress)
                           values (@FirstName, @LastName, @EmailAddress);";

            //return SqlDataAccess.SaveData(sql, data);
            return NoDatabaseYet(sql, data);
        }

        public static List<CustomerModel> LoadCustomers()
        {
            string sql = @"select Id, FirstName, LastName, EmailAddress
                           from dbo.Customer;";

            //return SqlDataAccess.LoadData<CustomerModel>(sql);
            return NoDatabaseYet(sql);
        }

        public static int NoDatabaseYet(string sql, CustomerModel data)
        {
            int defaultReturn = 0;
            try
            {
                defaultReturn = SqlDataAccess.SaveData(sql, data);
            }
            catch (Exception e)
            {
                var showme = e.Message;
            }
            return defaultReturn;
        }

        public static List<CustomerModel> NoDatabaseYet(string sql)
        {
            List<CustomerModel> defaultCustomer = null;
            try
            {
                defaultCustomer = SqlDataAccess.LoadData<CustomerModel>(sql);
            }
            catch (Exception e)
            {
                var showme = e.Message;
            }
            return defaultCustomer;
        }
    }
}
