﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLibrary.DataAccess
{
    public static class SqlDataAccess
    {
        public static string GetConnectionString(string connectionName = "MVCDemoDB")
        {
            return ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">This is your data model</typeparam>
        /// <param name="sql">This can be a Stored proc, or maybe inline sql</param>
        /// <returns></returns>
        public static List<T> LoadData<T>(string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                return cnn.Query<T>(sql).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">This is your data model</typeparam>
        /// <param name="sql">This can be a Stored proc, or maybe inline sql</param>
        /// <returns></returns>
        public static int SaveData<T>(string sql, T data)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                return cnn.Execute(sql, data);
            }
        }

        public static void UpdateData<T>(T person, string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                cnn.Execute(sql, person);
            }
        }
    }
}
