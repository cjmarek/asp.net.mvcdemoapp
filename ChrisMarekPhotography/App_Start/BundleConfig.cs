﻿using System.Web;
using System.Web.Optimization;

namespace ChrisMarekPhotography
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                     "~/Scripts/lib/toastr.js",
                     "~/Scripts/lib/jquery-{version}.js",
                     "~/Scripts/lib/jquery-ui.js",
                     "~/Scripts/slideshow.js",
                     "~/Scripts/Portfolio.js",
                     "~/Scripts/images.js",
                     "~/Scripts/about.js",
                     "~/Scripts/shared.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/lib/jquery.validate.js",
                        "~/Scripts/lib/jquery.validate.unobtrusive.js"
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/lib/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/toastr.css",
                      "~/Content/jquery-ui.css"
             ));
        }
    }
}
