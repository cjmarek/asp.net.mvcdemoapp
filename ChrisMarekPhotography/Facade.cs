﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChrisMarekPhotography.Common;
//using System.Web.Caching;
using ChrisMarekPhotography.Models;

namespace ChrisMarekPhotography
{
    public class Facade : IFacade
    {
        private const string PORTFOLIO = "portfolio";
        private const string SLIDESHOW = "slideshow";
        private const string DETAILS = "details";
        private const string INDEXES = "indexes";
        private IDao _dao;


        public Facade()
        {

        }

        internal Facade(IDao dao)
        //: this()
        {
            this._dao = dao;
        }

        private IDao GetDao()
        {
            //return _dao ?? Factory.CreateDao();
            if (_dao == null)
            {
                _dao = Factory.CreateDao();
            }
            return _dao;
        }

        public void SetDao(IDao dao)
        {
            _dao = dao;
        }

        /// <summary>
        /// HttpContext.Current is not ever available durring the unit test. 
        /// So durring unit test this method will always return false;
        /// </summary>
        /// <returns></returns>
        public bool IsCachingOn()
        {
            bool result = true;
            object sessionValue = null;

            if (HttpContext.Current == null)
            {
                result = false;
            }
            else
            {
                sessionValue = HttpContext.Current.Session[Controllers.ControllerBase.SessionKeys.ApplicationCacheKey];

                if (sessionValue == null)
                {
                    HttpContext.Current.Session[Controllers.ControllerBase.SessionKeys.ApplicationCacheKey] = true;
                }
                else
                {
                    result = (bool)sessionValue;
                }
            }
            return result;
        }

        public IPhotosResult GetValidationResults()
        {
            IPhotosResult validatedPhotosResult = null;
            //IPhotosResult aggregate = Factory.CreatePhotosResult();
            IPhotosResult aggregate = new PhotosResult();
            validatedPhotosResult = GetValidatedFolders();
            aggregate.MergeFrom(validatedPhotosResult);
            validatedPhotosResult = GetDuplicateFolderIndexEntries();
            aggregate.MergeFrom(validatedPhotosResult);
            return aggregate;
        }

        /// <summary>
        /// Return a PhotoResult that can reveal which folders contain
        /// duplicate photo indexes so that we can tell the site administrator
        /// to clean it up.
        /// </summary>
        /// <returns></returns>
        public IPhotosResult GetDuplicateFolderIndexEntries()
        {
            IPhotosResult photosResult = null;
            if (IsCachingOn())
            {
                photosResult = IsDuplicateResultsInCache();
            }
            else
            {
                photosResult = ValidateDupPhotoIndexInFolders();
            }
            return photosResult;
        }

        /// <summary>
        /// Return a PhotoResult that can reveal which folders are invalid
        /// so that we don't try to use them
        /// </summary>
        /// <returns></returns>
        public IPhotosResult GetValidatedFolders()
        {
            IPhotosResult validatedPhotosResult = null;
            if (IsCachingOn())
            {
                validatedPhotosResult = IsValidationResultsInCache();
            }
            else
            {
                validatedPhotosResult = ValidateAllFolders();
            }

            return validatedPhotosResult;
        }

        /// <summary>
        /// Basic validation.
        /// </summary>
        /// <returns></returns>
        public IPhotosResult ValidateAllFolders()
        {
            //IPhotosResult aggregate = Factory.CreatePhotosResult();
            IPhotosResult aggregate = new PhotosResult();

            foreach (string folder in GetDao().GetAllPhotoFolders())
            {
                IPhotosResult validationResult = null;

                foreach (string file in GetDao().GetPhotoFolderFiles(folder))
                {
                    validationResult = ValidatePhotosFolder(folder, file);
                    aggregate.MergeFrom(validationResult);
                }
                InitializeValidFolders(aggregate, validationResult, folder);
            }
            return aggregate;
        }

        public void InitializeValidFolders(IPhotosResult aggregate, IPhotosResult validationResult, string folder)
        {
            if (validationResult.HasErrors == false)
            {
                aggregate.AddValidFolder(folder);
            }
        }

        //Rules:
        //Check to see if the files exist inside the folder
        //Check to see if the data in the file is good
        //Otherwise the folder is invalid and will need to be ignored when the application runs
        //(Only photographs in valid folders will show up on the web site)
        public IPhotosResult ValidatePhotosFolder(string folder, string fileName)
        {
            string dataPath = GetPath(folder, fileName);
            IPhotosResult photosResult = ValidatePhotoFolderFile(dataPath, fileName, folder);
            ValidatePhotoFolderData(photosResult, fileName, folder);

            return photosResult;
        }

        /// <summary>
        /// The only thing this method does is determine if the file exists. If the file 
        /// can't be located then an error is thrown and makes an entry into the error messages.
        /// </summary>
        /// <param name="IndexPath"></param>
        /// <param name="dataFile"></param>
        /// <param name="folder"></param>
        /// <returns></returns>
        public IPhotosResult ValidatePhotoFolderFile(string IndexPath, string fileName, string folder)
        {
            //IPhotosResult photosResult = Factory.CreatePhotosResult();
            IPhotosResult photosResult = new PhotosResult();

            try
            {
                photosResult.stringArray = GetPhotoFolderData(IndexPath);
            }
            catch (Exception e)
            {
                InitializePhotoResultErrorMessages(photosResult, fileName, folder, e.Message);
            }
            return photosResult;
        }

        public IPhotosResult ValidatePhotoFolderData(IPhotosResult photosResult, string dataFile, string dataFolder)
        {
            try
            {
                CheckDataForErrors(photosResult, dataFile, dataFolder);
            }
            catch (Exception e)
            {
                InitializePhotoResultErrorMessages(photosResult, dataFile, dataFolder, e.Message);
            }

            return photosResult;
        }

        /// <summary>
        /// Any uncaught error here will exit immediately and return to the try catch that wraps this method.
        /// </summary>
        /// <param name="folder">details, 
        /// there should be 4 elements in the details array.
        /// there should be at least one, non blank element in the indexes array.
        /// </param>
        /// <param name="photosResult"></param>InitializePhotoResultErrorMessages
        public void CheckDataForErrors(IPhotosResult photosResult, string dataFile, string dataFolder)
        {
            switch (dataFile)
            {
                case DETAILS:
                    try
                    {
                        if (photosResult.stringArray[0] == null ||
                            photosResult.stringArray[1] == null ||
                            photosResult.stringArray[2] == null ||
                            photosResult.stringArray[3] == null)
                        { }
                    }
                    catch (Exception e)
                    {
                        InitializePhotoResultErrorMessages(photosResult, dataFile, dataFolder, e.Message);
                        // new Exception($"Your details file is invalid (or missing) in folder: {dataFolder}.");
                    }
                    break;
                case INDEXES:
                    if (photosResult.stringArray.Count() < 1)
                    {
                        photosResult.AddErrors($"No valid indexes in folder: '{dataFolder}'.");
                        //throw new Exception($"No valid indexes in folder: {dataFolder}.");
                    }
                    foreach (string s in photosResult.stringArray)
                    {
                        if (s == null || s == string.Empty)
                        {
                            photosResult.AddErrors($"Invalid or missing indexe(s) in folder: '{dataFolder}'.");
                            //throw new Exception($"Invalid or missing indexe(s) in folder: {dataFolder}.");
                        }
                        if (int.TryParse(s, out var index) == false)
                        {
                            photosResult.AddErrors($"The CSV value: '{s}', in the indexes.txt file is invalid in folder: {dataFolder}.");
                            //throw new Exception($"Invalid or missing indexe(s) in folder: {dataFolder}.");
                        }
                    }
                    break;
                default:
                    photosResult.AddErrors($"The file: '{dataFile}', is not a valid file name. Folder: '{dataFolder}'");
                    break;
                    //throw new Exception("unexpected errors detected.");
            }
        }

        public void InitializePhotoResultErrorMessages(IPhotosResult photosResult, string dataFile, string dataFolder, string failureDescription)
        {
            photosResult.AddErrors($"Your {dataFile} file is invalid (or missing) in folder: {dataFolder}.");
            photosResult.AddErrors(failureDescription);
        }

        public IPhotosResult ValidateDupPhotoIndexInFolders()
        {
            //IPhotosResult photosResult = Factory.CreatePhotosResult();
            IPhotosResult photosResult = new PhotosResult();

            foreach (string error in CheckForPhotographDuplicates())
            {
                photosResult.AddErrors(error);
            }

            return photosResult;
        }

        /// <summary>
        /// The error this method is looking for is whenever somebody has accidentally entered the same index 
        /// into other folders. For example, if we have a bluebird folder of bluebird indexes, and lets say one of those
        /// indexes is 15. And then lets say we have a woodpecker folder of woodpecker indexes, and by accident one of
        /// those indexes is also 15. That is an error since the same index cannot refer back to different birds. This
        /// error can be detected here because as I create the dictionary by iterating thru the Photograph List (I created
        /// earlier) and when I try to add the Photograph to the dictionary using the index as a key, the dictionary 
        /// will throw an error if the same index is attempted twice.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> CheckForPhotographDuplicates()
        {
            Dictionary<string, IPhotograph> photographDictionary = new Dictionary<string, IPhotograph>();
            List<IPhotograph> photographs = GetPhotographList();

            foreach (IPhotograph photograph in photographs)
            {
                string errorResult = AddPhotoToDictionary(photograph, photographDictionary);
                if (errorResult != string.Empty)
                {
                    yield return errorResult;
                }
            }
        }

        public Dictionary<string, IPhotograph> GetPhotographDictionary()
        {
            Dictionary<string, IPhotograph> photographDictionary = new Dictionary<string, IPhotograph>();
            List<IPhotograph> photographs = GetPhotographList();

            foreach (IPhotograph photograph in photographs)
            {
                AddPhotoToDictionary(photograph, photographDictionary);
            }

            return photographDictionary;
        }
        public string AddPhotoToDictionary(IPhotograph p, Dictionary<string, IPhotograph> photographDictionary)
        {
            string result = string.Empty;

            try
            {
                string key = p.index;
                photographDictionary.Add(key, p);
                //photographDictionary[key] = p;  this also works (amazingly)
            }
            catch (Exception)
            {
                photographDictionary.TryGetValue(p.index, out var photograph);
                result = $"index '{p.index}',{p.folder}, already exists in folder : '{photograph.folder}'";
            }
            return result;
        }

        /// <summary>
        /// Don't confuse this with the Photograph Dictionary.
        /// I have two ways to organize photos. The photograph dictionary and the
        /// photograph List
        /// </summary>
        /// <returns></returns>
        public List<IPhotograph> GetPhotoList()
        {
            List<IPhotograph> photographs = CreatePhotographList();
            return photographs;
        }

        /// <summary>
        /// To make dealing with the random Indexes in the "portfolio" or "slideshow" folders easier, I will create a
        /// Photograph dictionary. See method GetPhotographDictionary(). I will use this Photograph List to do that.
        /// The dictionary will allow more flexibility (than the photograph List) when dealing with the indexes in the 
        /// "portfolio" or "slideshow" folders.
        /// </summary>
        /// <returns></returns>
        public List<IPhotograph> CreatePhotographList()
        {
            List<IPhotograph> photographs = new List<IPhotograph>();
            IPhotosResult ValidatedFoldersResult = GetValidatedFolders();
            string dataPath = string.Empty;

            foreach (string folder in ValidatedFoldersResult.ValidFolders)
            {
                if (folder != PORTFOLIO && folder != SLIDESHOW)
                {
                    dataPath = GetPath(folder, INDEXES);
                    string[] currentFolderIndexes = GetPhotoFolderData(dataPath);
                    LoadPhotographList(folder, photographs, currentFolderIndexes);
                }
            }
            return photographs;
        }
        /// <summary>
        /// this is what ties everything together. The photoindexes are associated 
        /// with the folder they belong with by being placed together in a photograph object.
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="photographs"></param>
        /// <param name="photoIndexes"></param>
        public void LoadPhotographList(string folder, List<IPhotograph> photographs, string[] photoIndexes)
        {
            foreach (string photoIndex in photoIndexes)
            {
                //IPhotograph photograph = Factory.CreatePhotograph();
                IPhotograph photograph = new Photograph();
                photograph.index = photoIndex;
                photograph.folder = folder;
                photographs.Add(photograph);
            }
        }

        public List<IPhotoInformation> PullImagesFromFolder(string folder)
        {
            List<IPhotoInformation> photoInformation = new List<IPhotoInformation>();
            IPhotosResult validatedPhotosResult = GetValidatedFolders();

            if (validatedPhotosResult.ValidFolders.Contains(folder) == true)
            {
                switch (folder)
                {
                    case PORTFOLIO:
                    case SLIDESHOW:
                        photoInformation = GetImageInfoForAssortedIndexes(folder);
                        break;
                    default:
                        photoInformation = GetImageInfoForDropDownFolder(folder);
                        break;
                }
            }
            return photoInformation;
        }

        public List<IPhotoInformation> GetImageInfoForDropDownFolder(string folder)
        {
            List<IPhotoInformation> photoInformation = new List<IPhotoInformation>();
            string IndexPath = GetPath(folder, INDEXES);
            string[] PhotoIndexes = GetPhotoFolderData(IndexPath);
            foreach (string photoIndex in PhotoIndexes)
            {
                GetPhotoInformation(photoInformation, folder, photoIndex);
            }
            return photoInformation;
        }

        /// <summary>
        /// The slideshow folder and portfolio folder are different then all the other folders.
        /// All the other folders contain indexes for just one specific subject.
        /// Also the slideshow folder and portfolio folder don't have a details.txt file in them.
        /// The slideshow and portfolio folders will contain indexes for any of the subjects. The following 
        /// method can look up a subject in the dictionary, to identify what folder that subject is in, and
        /// then look at the details.txt file for that subject (in that folder). The details file is where
        /// the 'mouse over' photo description comes from.
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        public List<IPhotoInformation> GetImageInfoForAssortedIndexes(string folder)
        {
            List<IPhotoInformation> photoInformation = new List<IPhotoInformation>();
            string dataPath = string.Empty;

            Dictionary<string, IPhotograph> photographDictionary = GetPhotosDictionary();
            //photo indexes come off in the same order as they are arranged in the indexes.txt file.
            dataPath = GetPath(folder, INDEXES);
            string[] photoIndexes = GetPhotoFolderData(dataPath);
            foreach (string index in photoIndexes)
            {
                if (photographDictionary.TryGetValue(index, out var photograph) == true)
                {
                    GetPhotoInformation(photoInformation, photograph.folder, photograph.index);
                }
            }
            return photoInformation;
        }

        // * * *  Not in use * * *, but this is a reminder that the application can work without 
        //the dictionary. This alternate approach does not need the dictionary. It is pulling the 
        //data from the photographs List<>.The problem is, that the photographs List was loaded by iterating 
        //thru the whole set of folders, in no particular order, and so the photographs end up 
        //showing in no particular order. This is annoying since I need to know that the images 
        //will show in the same order they occur in the slideshow and portfolio folders. This is the reason for  
        //using the dictionary in the above method, to be able to iterate over the indexes 
        //in the folders so that the image order comes off as expected. 
        private List<IPhotoInformation> GetImagesFromPhotographsList(string folder)
        {
            List<IPhotoInformation> photoInformation = new List<IPhotoInformation>();
            IPhotosResult validatedPhotosResult = GetValidatedFolders();
            List<IPhotograph> photographs = GetPhotographList();

            foreach (Photograph photograph in photographs)
            {
                if (validatedPhotosResult.ValidFolders.Contains(photograph.folder) == true)
                {
                    GetPhotoInformation(photoInformation, photograph.folder, photograph.index);
                }
            }
            return photoInformation;
        }

        private void GetPhotoInformation(List<IPhotoInformation> photoInformation, string folder, string index)
        {
            string dataPath = GetPath(folder, DETAILS);
            string[] Details = GetPhotoFolderData(dataPath);
            if (int.TryParse(index, out var imageIndex))
            {
                //photoInformation.Add(Factory.CreatePhotoInformation(Details[0], Details[1], Details[2], Details[3], imageIndex));
                photoInformation.Add(new PhotoInformation(Details[0], Details[1], Details[2], Details[3], imageIndex));
            }
        }

        /// <summary>
        /// Return a complete List of indexes for all the photographs in the valid folders.
        /// However, ignore folders that have been determined to have something wrong.
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllPhotoIndexes()
        {
            IPhotosResult ValidatedFoldersResult = GetValidatedFolders();
            List<string> allIndexes = new List<string>();

            string dataPath = string.Empty;

            foreach (string folder in ValidatedFoldersResult.ValidFolders)
            {
                if (folder != PORTFOLIO && folder != SLIDESHOW)
                {
                    dataPath = GetPath(folder, INDEXES);
                    string[] photoIndexes = GetPhotoFolderData(dataPath);
                    foreach (string photoIndex in photoIndexes)
                    {
                        allIndexes.Add(photoIndex);
                    }
                }
            }
            return allIndexes;
        }

        public string[] GetPhotoFolderData(string IndexPath)
        {
            string[] result = GetDao().GetArray(IndexPath);
            return result;
        }

        public string GetPath(string folder, string dataFile)
        {
            IPathManager pathManager = Factory.CreateServerPathManager();
            string IndexPath = pathManager.GetPath() + @"photos\" + folder + @"\" + dataFile + @".txt";
            return IndexPath;
        }

        public IPhotoViewModel GetImages(string dataFolder)
        {
            IPhotoViewModel photoViewModel = null;

            if (IsCachingOn())
            {
                //photoViewModel = Factory.CreatePhotoViewModel(IsImagesInCache(dataFolder));
                photoViewModel = new PhotoViewModel(IsImagesInCache(dataFolder));
            }
            else
            {
                //photoViewModel = Factory.CreatePhotoViewModel(PullImagesFromFolder(dataFolder));
                photoViewModel = new PhotoViewModel(PullImagesFromFolder(dataFolder));
            }
            return photoViewModel;
        }

        public Dictionary<string, IPhotograph> GetPhotosDictionary()
        {
            Dictionary<string, IPhotograph> photographDictionary = null;
            if (IsCachingOn())
            {
                photographDictionary = IsPhotosDictionaryInCache();
            }
            else
            {
                photographDictionary = GetPhotographDictionary();
            }
            return photographDictionary;
        }

        public List<IPhotograph> GetPhotographList()
        {
            List<IPhotograph> photographs = null;
            if (IsCachingOn())
            {
                photographs = IsPhotographListInCache();
            }
            else
            {
                photographs = GetPhotoList();
            }
            return photographs;
        }
        public List<string> GetAllPhotographIndexes()
        {
            List<string> photographIndexes = null;
            if (IsCachingOn())
            {
                photographIndexes = IsPhotographIndexesInCache();
            }
            else
            {
                photographIndexes = GetAllPhotoIndexes();
            }
            return photographIndexes;
        }

        private List<IPhotoInformation> IsImagesInCache(string folder)
        {
            TimeSpan tsExpires = new TimeSpan(0, 12, 0);
            string key = ($"__{folder}_photos");
            LoadKeysIntoList(key);
            List<IPhotoInformation> photoInformation = ReadThroughSlidingCache(key, () => PullImagesFromFolder(folder), tsExpires);
            return photoInformation;
        }
        private IPhotosResult IsValidationResultsInCache()
        {
            TimeSpan tsExpires = new TimeSpan(0, 12, 0);
            string key = "__Validated_photos";
            LoadKeysIntoList(key);
            IPhotosResult photosResult = ReadThroughSlidingCache(key, () => ValidateAllFolders(), tsExpires);
            return photosResult;
        }

        private List<IPhotograph> IsPhotographListInCache()
        {
            TimeSpan tsExpires = new TimeSpan(0, 12, 0);
            string key = "__PhotographList";
            LoadKeysIntoList(key);
            List<IPhotograph> photoList = ReadThroughSlidingCache(key, () => GetPhotoList(), tsExpires);
            return photoList;
        }
        private List<string> IsPhotographIndexesInCache()
        {
            TimeSpan tsExpires = new TimeSpan(0, 12, 0);
            string key = "__PhotographIndexes";
            LoadKeysIntoList(key);
            List<string> photoIndexes = ReadThroughSlidingCache(key, () => GetAllPhotoIndexes(), tsExpires);
            return photoIndexes;
        }

        private Dictionary<string, IPhotograph> IsPhotosDictionaryInCache()
        {
            TimeSpan tsExpires = new TimeSpan(0, 12, 0);
            string key = "__PhotosDictionary";
            LoadKeysIntoList(key);
            Dictionary<string, IPhotograph> photographDictionary = ReadThroughSlidingCache(key, () => GetPhotographDictionary(), tsExpires);
            return photographDictionary;
        }

        private IPhotosResult IsDuplicateResultsInCache()
        {
            TimeSpan tsExpires = new TimeSpan(0, 12, 0);
            string key = "__Duplicated_photos";
            LoadKeysIntoList(key);
            IPhotosResult photosResult = ReadThroughSlidingCache(key, () => ValidateDupPhotoIndexInFolders(), tsExpires);
            return photosResult;
        }

        /// <summary>  
        /// This method keeps track of the cache keys and will be used to purge from the cache everything that has been added
        /// while leaving other necessary items in the cache that support the application.
        /// </summary>
        /// <param name="key"></param>
        private void LoadKeysIntoList(string key)
        {
            List<string> keys = GetKeysAddedtoTheCache();
            if (keys.Contains(key) == false) { keys.Add(key); }
        }

        private List<string> GetKeysAddedtoTheCache()
        {
            TimeSpan tsExpires = new TimeSpan(0, 12, 0);
            string key = "__Keys";
            List<string> keys = ReadThroughSlidingCache(key, () => GetKeys(), tsExpires);
            return keys;
        }

        /// <summary>
        /// adding a List<string> to the cache(if it is not already there). Afterwards, each time 
        /// LoadKeysIntoList() is called, LoadKeysIntoList brings back a reference to this 
        /// List<string> key, so that LoadKeysIntoList() can add the latest
        /// key to it(the reference to the keys object in the cache).
        /// </summary>
        /// <returns></returns>
        public List<string> GetKeys()
        {
            List<string> keys = new List<string>();
            return keys;
        }

        /// <summary>
        ///sliding expiration (time gets extended if another request comes in before last one expired)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="func"></param>
        /// <param name="tsExpires"></param>
        /// <returns></returns>
        public T ReadThroughSlidingCache<T>(string key, Func<T> func, TimeSpan tsExpires)
        {
            T result = default(T);
            var Cache = HttpContext.Current.Cache;
            object cached = Cache[key];

            if (cached == null)
            {
                result = func();
                Cache.Insert(key, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, tsExpires);
            }
            else
            {
                result = (T)cached;
            }
            return result;
        }

        public void ClearCachedItems()
        {
            List<string> keys = GetKeysAddedtoTheCache();
            lock (HttpContext.Current.Cache)
            {
                for (int i = 0; i < keys.Count; i++)
                    HttpContext.Current.Cache.Remove(keys[i]);
                //And now get rid of the __Key for the keys.
                HttpContext.Current.Cache.Remove("__Keys");
            }
        }
    }
}




