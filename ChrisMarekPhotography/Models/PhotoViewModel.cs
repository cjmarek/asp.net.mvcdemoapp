﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChrisMarekPhotography.Models
{
    public class PhotoViewModel : IPhotoViewModel
    {
        public PhotoViewModel(List<IPhotoInformation> photoInfo)
        {
            PhotoInformation = photoInfo;
        }

        public List<IPhotoInformation> PhotoInformation { get; set; }
    }
}