﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;


namespace ChrisMarekPhotography.Models
{
    [Serializable]
    public class PhotosResult : IPhotosResult
    {
        private List<string> errors = new List<string>();
        private List<string> validFolders = new List<string>();
        public PhotosResult()
        {

        }


        /// <summary>
        /// ReadOnly Properties to expose errors
        /// </summary>
        public IEnumerable<string> ErrorMessages
        {
            get
            {
                return this.errors;
            }
        }
        /// <summary>
        /// ReadOnly Properties to expose Valid Folders
        /// </summary>
        public IEnumerable<string> ValidFolders
        {
            get
            {
                return this.validFolders;
            }
        }

        public List<IPhotoInformation> photoInformation { get; set; } = new List<IPhotoInformation>();
        public string[] stringArray { get; set; }

        // Params keyword in C# can be used to declare methods that do not know the number of parameters coming in. 
        // Params are also useful to write "clean code". Instead of using various overloaded methods to pass multiple values, 
        // we can simply create an array and pass it as an argument or a comma separated list of values.
        public void AddErrors(string failure, params object[] args)
        {
            errors.Add(failure);
        }

        public void AddValidFolder(string folder)
        {
            validFolders.Add(folder);
        }

        public bool HasErrors
        {
            get
            {
                return errors.Count > 0;
            }
        }

        public void MergeFrom(IPhotosResult fromResult)
        {
            foreach (string message in fromResult.ErrorMessages)
            {
                this.errors.Add(message);
            }
            foreach (string folder in fromResult.ValidFolders)
            {
                this.validFolders.Add(folder);
            }
        }
    }
}