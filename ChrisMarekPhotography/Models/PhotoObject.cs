﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChrisMarekPhotography.Models
{
    [Serializable]
    public class PhotoObject : IPhotoObject
    {
        public PhotoObject(string[] details, string[] photoIndexes)
        {
            Details = details;
            PhotoIndexes = photoIndexes;
        }
        public string[] Details { get; set; }
        public string[] PhotoIndexes { get; set; }
    }
}