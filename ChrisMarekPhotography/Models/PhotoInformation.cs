﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChrisMarekPhotography.Models
{
    public class PhotoInformation : IPhotoInformation
    {
        //I found out the hard way that you can't create names like 0.jpg, 1.jpg 2.jpg because chrome acts weird with such names.
        //However 0a.jpg, 1a.jpg works fine in chrome
        public PhotoInformation(string imageDescription, string location, string type, string specificType, int? index)
        {
            ImageDescription = imageDescription;
            Location = location;
            Type = type;
            SpecificType = specificType;
            Index = index;
            ImagePath = "~/Images/" + index.ToString() + "a.jpg";
        }


        public string ImageDescription { get; set; }

        public string Location { get; set; }

        public string Type { get; set; }

        public string SpecificType { get; set; }

        public int? Index { get; set; }

        public string ImagePath
        {
            get;

            set;
        }
    }
}