﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChrisMarekPhotography
{
    public class Dao : IDao
    {
        /// <summary>
        /// This method is used to read comma separated text files (details.txt and indexes.txt)
        /// returns a string array from a comma separated text file
        /// </summary>
        public string[] GetArray(string path)
        {
            IFileReaderManager fr = Factory.CreateFileReaderManager();
            var array = fr.FileReader(path);
            return array;
        }

        /// <summary>
        /// returns directory names (folder names)
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetAllPhotoFolders()
        {
            int lastIndex = 0;
            string folderName = string.Empty;
            IDirectoriesManager dm = Factory.CreateDirectoriesManager();

            var directories = dm.GetAllPhotoFolders();
            foreach (string folderPath in directories)
            {
                lastIndex = folderPath.LastIndexOf(@"\");
                folderName = folderPath.Substring(lastIndex + 1);
                yield return folderName;
            }
        }

        /// <summary>
        /// returns file names (indexes.txt And details.txt files)
        /// from the folder.
        /// Note: portfolio and slideshow folders will only contain an indexes.txt file.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetPhotoFolderFiles(string folder)
        {
            int lastIndex = 0;
            string fileName = string.Empty;
            IFolderFilesManager ffm = Factory.CreateFolderFilesManager();

            string[] files = ffm.GetPhotoFolderFiles(folder);
            foreach (string file in files)
            {
                lastIndex = file.LastIndexOf(@"\");
                fileName = file.Substring(lastIndex + 1);
                fileName = fileName.Split('.')[0];
                yield return fileName;
            }
        }
    }
}