﻿var Photography = Photography || {};
Photography.slideshow = (function () {
    "use strict";

    return {

        init: function () {
            console.log("Initializing Photography.slideshow...");

            var slideShowAnimation = function () {
                /* SET PARAMETERS */
                var change_image_time = 5000;
                var transition_speed = 1000;

                $("img").fadeIn(transition_speed);
                var slideshow = $(".slideshow_grid").fadeIn(1000),
                    listItems = slideshow.children('li'),
                    listLen = listItems.length,
                    i = 0,
                    changeList = function () {

                        listItems.eq(i).fadeOut(transition_speed, function () {
                            i += 1;
                            if (i === listLen) {
                                i = 0;
                            }
                            listItems.eq(i).fadeIn(transition_speed);
                        });
                    };

                listItems.not(':first').hide();
                setInterval(changeList, change_image_time);
                $("#loading-spinner").fadeTo(1000, 0);
            };

            var loadSlideShowImages = function () {
                $.ajax({
                    url: urlMap.Ajax.GetSlideShowImages,
                    data: { folder: "slideshow" },
                    success: function (results) {
                        var self = $(".slideshow_grid").hide();
                        self.html(results);
                        slideShowAnimation();
                        $("#footer").show(); 
                    }
                });
            };

            loadSlideShowImages();


            var slideShowlink = function () {
                $('.slideshow-link').addClass("disabled");
                $('.slideshow-link').css('color', 'black');
                $('.slideshow-link').text("Home");
                $('.slideshow-link').fadeTo(1000, 0.1);
            };

            slideShowlink();

            //Newsletter sign-up submission
            Photography.shared.manageSubmitButton();
            //protect copyright
            Photography.shared.preventRightClick();

        }
    };
}());   
