﻿var Photography = Photography || {};
Photography.images = (function () {
    "use strict";

    var defaultDialogOptions = {
        autoOpen: false,
        show: "fade",
        modal: true,
        hide: "fadeOut",
        draggable: true,
        resizable: false,
        closeOnEscape: true,
        width: "auto",
        height: "auto"
    };

    var dropDownSelection = { dropDownSelection: null };

    return {

        init: function () {
            console.log("Initializing Photography.images...");

            dropDownSelection.dropDownSelection = $(".image_select-container");
            $('select option:odd').css({ 'background-color': 'beige' });
            $("#view_image_dialog").dialog(
                $.extend({
                    buttons: {
                        Close: function () {
                            $(this).dialog("close");
                        }
                    }
                }, defaultDialogOptions));

            var viewDocument = function (documentId) {
                var src = "/Images/" + documentId + "a.jpg";
                $("#view_image_content").prop("src", src);
                $("#view_image_dialog").dialog("open");
            };

            var wireUpClicks = function () {
                $(".images_grid").find('li').click(function () {
                    toastr.info("copy right protected");
                    if ($(this).data("property") != undefined) {
                        viewDocument($(this).data("property"))
                    }
                });
            };

            var wireUpImageSelectClick = function () {
                $("#image_select").click(function () {
                    loadImagesSearch(dropDownSelection.dropDownSelection.find("select[data-property='image_select']").val());
                });
            };
            wireUpImageSelectClick();


            var loadImagesSearch = function (imagesFolder) {
                $.ajax({
                    url: urlMap.Ajax.SearchImages,
                    data: { folder: imagesFolder },
                    success: function (results) {
                        $("#loading-spinner").fadeTo(1000, 0);
                        var self = $(".images_grid");
                        self.html(results + "<li></li>").hide();
                        self.fadeIn(1000);
                        $("#footer").show(); 
                        wireUpClicks();
                    }
                });
            };

            var imageslink = function () {
                $('.images-link').addClass("disabled");
                $('.images-link').css('color', 'black');
                $('.images-link').text("Images");
                $('.images-link').fadeTo(1000, 0.1);
            };

            imageslink();

            loadImagesSearch("greatblueheron");

            //Newsletter sign-up submission
            Photography.shared.manageSubmitButton();
            //protect copyright
            Photography.shared.preventRightClick();


        } 
    };   
}());   
