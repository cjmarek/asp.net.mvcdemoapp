﻿/// <reference path="lib/toastr.js" /> 

/*global 
urlMap:false,
XRegExp:false,
_:false,
$:false,
moment:false,
toastr:false,
console:true
*/


var Photography = Photography || {};
var console = console || {
    log: function () { },
    error: function (errorString) { alert(errorString); },
    warn: function () { }
}; 

Photography.shared = (function () {
    "use strict";

    $.fn.makeSelectable = function (multi) {
        /// <summary>Makes a jQuery element selectable by applying the selected style and removing the selected style from siblings.</summary>
        /// <param name="multi" type="Object"></param>
        return this.click(function () {
            $(this).addClass('selected');

            if (!multi) {
                $(this).siblings().removeClass('selected').end();
            }
        });
    };

    $.fn.noSelect = function () {
        /// <summary>prevent text selection</summary>
        var a = 'none';
        return this.bind('selectstart dragstart mousedown', function () { return false; })
            .css({ MozUserSelect: a, msUserSelect: a, webkitUserSelect: a, userSelect: a });
    };

    return {

        handleError: function (e, xhr, settings, error) {
            var errorString = '<div>There was a problem submitting the request.<div>',
                isCustomError = xhr.responseText && xhr.responseText.indexOf("custom-error") !== -1,
                closeError = function (event) {
                    $("#error_container").hide();
                    $("#error_detail_frame").contents().find("html").html("");
                };

            if (isCustomError) {
                errorString += '<div style="text-decoration: underline">Click here for details</div>';
            }

            var toast = toastr.error(errorString, "Error");

            if (isCustomError) {
                toast.click(function () {
                    var frame = $("#error_detail_frame");

                    frame.contents().find("html").html(xhr.responseText);
                    frame.width($(window).width());
                    frame.height($(window).height() + 14);

                    var container = $("#error_container").css({ position: "absolute", top: -20, left: 0 });

                    $(document).one("keydown", closeError);
                    $("#error_detail_frame").contents().one("keydown", closeError);

                    container.show();
                });
            }
        },

        handleAjaxErrors: function () {
            /// <summary>Attaches a default jQuery ajaxError() handler which shows a clickable toast.</summary>
            console.log("Attaching toastr handler for ajax errors...");
            $.extend(toastr.options, {
                positionClass: "toast-top-right",
                fadeIn: 300,
                fadeOut: 750
            });
            //Whenever an Ajax request completes with an error, jQuery triggers the ajaxError event. 
            //Any and all handlers that have been registered with the .ajaxError() method are executed 
            //at this time.
            $(document).ajaxError(this.handleError);
        },

        manageSubmitButton: function () {
            console.log("Initializing submit button...");

            var fields = "#FirstName, #LastName, #EmailAddress, #ConfirmEmail";
            $(fields).on('change', function () {
                //this causes annoying button problem. 
                //if (allFilled()) {
                //    $('#register').removeAttr('disabled');
                //} else {
                //    $('#register').attr('disabled', 'disabled');
                //}
            });

            function allFilled() {
                var filled = true;
                $(fields).each(function () {
                    if ($(this).val() == '') {
                        filled = false;
                    }
                });
                return filled;
            }
            $("#register").on('click', function () {
                var validator = $("div.signup span").hasClass("field-validation-error");
                if (validator == true || allFilled() == false) {
                    toastr.error("please fix Newsletter Sign-up errors");
                }
                else {
                    toastr.info("Newsletter registered succesfully");
                }
            });
        },
        preventRightClick: function () {
            console.log("Initializing right click prevent...");
            //return;
            window.addEventListener('contextmenu', function (e) {
                toastr.info("copy right protected");
                e.preventDefault();
            }, false);
        }
    };
}());