var Photography = Photography || {};
Photography.portfolio = (function () {
    "use strict";

    var defaultDialogOptions = {
        autoOpen: false,
        show: "fade",
        modal: true,
        hide: "fadeOut",
        draggable: true,
        resizable: false,
        closeOnEscape: true,
        width: "auto",
        height: "auto"
    };

    var btns = {
        btn1: "all",
        btn2: "birds",
        btn3: "butterflies",
        btn4: "insects",
        btn5: "flora",
        btn6: "landscape"
    }


    return {

        init: function () {
            console.log("Initializing Photography.process...");

            $("#btn1").click(function () {
                selection(btns.btn1);
                $("#btn2").show();
                $("#btn3").show();
                $("#btn4").show();
                $("#btn5").show();
                $("#btn6").show();
            });
            $("#btn2").click(function () {
                selection(btns.btn2);
                $("#btn3").hide();
                $("#btn4").hide();
                $("#btn5").hide();
                $("#btn6").hide();
            });
            $("#btn3").click(function () {
                selection(btns.btn3);
                $("#btn2").hide();
                $("#btn4").hide();
                $("#btn5").hide();
                $("#btn6").hide();
            });
            $("#btn4").click(function () {
                selection(btns.btn4);
                $("#btn2").hide();
                $("#btn3").hide();
                $("#btn5").hide();
                $("#btn6").hide();
            });
            $("#btn5").click(function () {
                selection(btns.btn5);
                $("#btn2").hide();
                $("#btn3").hide();
                $("#btn4").hide();
                $("#btn6").hide();
            });
            $("#btn6").click(function () {
                selection(btns.btn6);
                $("#btn2").hide();
                $("#btn3").hide();
                $("#btn4").hide();
                $("#btn5").hide();
            });

            $("#view_image_dialog").dialog(
                $.extend({
                    buttons: {
                        Close: function () {
                            $(this).dialog('close');
                        }
                    }
                }, defaultDialogOptions));

            var selection = function (btn) {
                var nameArray, i;
                nameArray = document.getElementsByClassName("column");
                if (btn == "all") btn = "";
                for (i = 0; i < nameArray.length; i++) {
                    RemoveClass(nameArray[i]);
                    nameArray[i].className.indexOf(btn) > -1 ? AddShowClass(nameArray[i]) : AddHideClass(nameArray[i]);
                }
            };

            var AddShowClass = function (element) {
                element.className += " " + "show";
            };

            var AddHideClass = function (element) {
                element.className += " " + "hide";
            };

            var RemoveClass = function (element) {
                var arr1;
                arr1 = element.className.split(" ");
                while (arr1.indexOf("show") > -1) {
                    arr1.splice(arr1.indexOf("show"), 1);
                }
                while (arr1.indexOf("hide") > -1) {
                    arr1.splice(arr1.indexOf("hide"), 1);
                }
                element.className = arr1.join(" ");
            };

            var viewDocument = function (documentId) {
                var src = "/Images/" + documentId + "a.jpg";
                $("#view_image_content").prop("src", src);
                $("#view_image_dialog").dialog("open");
            };

            var wireUpClicks = function () {
                $(".images_grid").find('li').click(function () {
                    toastr.info("copy right protected");
                    if ($(this).data("property") != undefined) {
                        viewDocument($(this).data("property"))
                    }
                });
            };

            var timeDelay = function (results) {
                var self = $(".images_grid");
                self.html(results + "<li></li>").hide();
                window.setTimeout(function () {
                    self.html(results + "<li></li>").fadeIn(2000);
                    $("#loading-spinner").fadeOut(1000, 0);
                    $("#footer").show();
                    wireUpClicks();
                }, 2000);
            };

            var loadImageRows = function () {
                $.ajax({
                    url: urlMap.Ajax.GetPortfolioImages,
                    data: { folder: "portfolio" },
                    success: function (results) {
                        timeDelay(results);
                    }
                });
            };

            var portfoliolink = function () {
                $('.portfolio-link').addClass("disabled");
                $('.portfolio-link').css('color', 'black');
                $('.portfolio-link').text("Portfolio");
                $('.portfolio-link').fadeTo(1000, 0.1);
            };

            portfoliolink();
            loadImageRows();
            selection("all");

            //Newsletter sign-up submission
            Photography.shared.manageSubmitButton();
            //protect copyright
            Photography.shared.preventRightClick();

            (function () {
                var btnContainer = document.getElementById("myBtnContainer");
                var btns = btnContainer.getElementsByClassName("btn");
                for (var i = 0; i < btns.length; i++) {
                    btns[i].addEventListener("click", function () {
                        var current = document.getElementsByClassName("active");
                        current[0].className = current[0].className.replace(" active", "");
                        this.className += " active";
                    });
                };
            }());
        }
    };
}());   
