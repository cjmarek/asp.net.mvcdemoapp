﻿var Photography = Photography || {};
Photography.about = (function () {
    "use strict";

    return {

        init: function () {
            console.log("Initializing Photography.about...");
            $("#footer").show(); 
            var imageslink = function () {
                $('.about-link').addClass("disabled");
                $('.about-link').css('color', 'black');
                $('.about-link').text("About");
                $('.about-link').fadeTo(1000, 0.1);
            };

            imageslink();
            //Newsletter sign-up submission
            Photography.shared.manageSubmitButton();
            //protect copyright
            Photography.shared.preventRightClick();
        } 
    };  
}());  
