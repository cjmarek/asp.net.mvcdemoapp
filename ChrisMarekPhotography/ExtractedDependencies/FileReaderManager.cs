﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChrisMarekPhotography.ExtractedDependencies
{
    /// <summary>
    /// This class is used for dependancy-breaking so that the application
    /// is testable
    /// This class returns string arrays of a file contents.
    /// i.e. for a details.txt file for bluebird, it returns a string array Sialia sialis,Irving Texas,birds,blue bird
    /// i.e. for a indexes.txt file for bluebird, it returns a string array 166,10,11,12,13,14,32,162,163,164,165,252,255,256,257,258,291,259,260,261
    /// </summary>
    public class FileReaderManager : IFileReaderManager
    {
        public string[] FileReader(string path)
        {
            Utilities.FileReader fr = new Utilities.FileReader(path);
            string[] array = fr.Read();
            fr.Close();
            return array;
        }
    }
}