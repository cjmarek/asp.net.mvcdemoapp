﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChrisMarekPhotography.ExtractedDependencies
{
    /// <summary>
    /// This class is used for dependancy-breaking so that the Dao.cs
    /// is testable
    /// This class returns the names of all folders under the photos folder
    /// i.e. bluebird, gecko, cicada, bison
    /// </summary>
    public class DirectoriesManager : IDirectoriesManager
    {
        /// <summary>
        /// Paths that start with a tilde-slash (~/) or a slash (/) refer to the app root:
        /// This class returns string arrays of physical path names from under the photos folder.
        /// "C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\photos\\acornwoodpecker"
        /// "C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\photos\\ameicancoot"
        /// "C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\photos\\ameicancoot"
        /// "C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\photos\\ameicancoot"
        ///   . . . and all the rest.
        /// </summary>
        /// <returns></returns>
        public string[] GetAllPhotoFolders()
        {
            var Server = HttpContext.Current.Server;
            string[] directories = Directory.GetDirectories(Server.MapPath("~") + @"photos");
            return directories;
        }
    }
}