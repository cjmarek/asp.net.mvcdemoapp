﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChrisMarekPhotography;

namespace ChrisMarekPhotography.ExtractedDependencies
{
    /// <summary>
    /// This class is used for dependancy-breaking so that the application
    /// is testable
    /// </summary>
    public class ServerPathManager : IPathManager
    {
        /// <summary>
        /// Paths that start with a tilde-slash (~/) or a slash (/) refer to the app root:
        /// This class returns a string of the physical path of the application.
        /// "C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\"
        /// </summary>
        /// <returns></returns>
        public string GetPath()
        {
            var Server = HttpContext.Current.Server;
            return Server.MapPath("~");
        }
    }
}