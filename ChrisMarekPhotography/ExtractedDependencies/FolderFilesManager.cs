﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChrisMarekPhotography.ExtractedDependencies
{
    /// <summary>
    /// This class is used for dependancy-breaking so that the Dao.cs
    /// is testable
    /// This class returns the files that are contained in a specific folder.
    /// i.e. folder bluebird contains files details.txt and indexes.txt
    /// </summary>
    public class FolderFilesManager : IFolderFilesManager
    {
        /// <summary>
        /// Paths that start with a tilde-slash (~/) or a slash (/) refer to the app root:
        /// This class returns string arrays of the physical paths for the files in a specfic folder.
        /// i.e. C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\\\photos\\acornwoodpecker\\details.txt"
        /// i.e. C:\\BitBucketDemoMvc\\asp.net.mvcdemoapp\\ChrisMarekPhotography\\\\photos\\acornwoodpecker\\indexes.txt"
        /// </summary>
        /// <returns></returns>
        public string[] GetPhotoFolderFiles(string folder)
        {
            var Server = HttpContext.Current.Server;
            string[] files = Directory.GetFiles(Server.MapPath("~") + @"\photos\" + folder);
            return files;
        }
    }
}