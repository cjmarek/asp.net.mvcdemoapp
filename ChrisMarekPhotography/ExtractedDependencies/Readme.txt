﻿Inside the Facade.cs there were DAO methods in the code that needed to interact with the file system or session or some other resources that
I have no control over. So in order to make code that consumes such 'resources' to be testable, I extracted those DAO file system resources into their
own class, and made those 'extracted classes' inherit from an Interface. Then I placed them all in a ExtractedDependencies folder. This enables loose coupling.
Now, in the Facade where ever these Dao methods were extracted, I can invoke a Factory class that supplies those 'extracted class instances'. But because
these classes have interfaces, I can choose to substitute test stub classes that use the same interface. 
They are going to be interchangeable if those classes implememt the same interface.

The class I extracted (real production code) or a 'fake' class (that also inherits from the same interface) can substitute
for the production code, and I can control what the fake class does. So I can create all kinds of test scenarios in the fake class.

