﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLibrary;
using static DataLibrary.BusinessLogic.CustomerProcessor;

namespace ChrisMarekPhotography.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// this is the portfolio view
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// this is the portfolio view
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Models.CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = CreateCustomer(
                    model.FirstName,
                    model.LastName,
                    model.EmailAddress);
                // return RedirectToAction("Index");  comment out to stay on current view
            }

            return View();
        }

        /// <summary>
        /// this is the slide show view
        /// </summary>
        /// <returns></returns>
        public ActionResult Home()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        /// <summary>
        /// this is the slide show view
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Home(Models.CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = CreateCustomer(
                    model.FirstName,
                    model.LastName,
                    model.EmailAddress);
                // return RedirectToAction("Index"); comment out to stay on current view
            }

            return View();
        }

        /// <summary>
        /// this is the drop down selection view
        /// </summary>
        /// <returns></returns>
        public ActionResult Images()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        /// <summary>
        /// this is the drop down selection view
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Images(Models.CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = CreateCustomer(
                    model.FirstName,
                    model.LastName,
                    model.EmailAddress);
                // return RedirectToAction("Index");  comment out to stay on current view
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }


        /// <summary>
        /// this is the about view
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult about(Models.CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = CreateCustomer(
                    model.FirstName,
                    model.LastName,
                    model.EmailAddress);
                // return RedirectToAction("Index");  comment out to stay on current view
            }

            return View();
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}