﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataLibrary;
using ChrisMarekPhotography.Models;
using static DataLibrary.BusinessLogic.CustomerProcessor;


namespace ChrisMarekPhotography.Controllers
{
    public class AjaxController : ControllerBase
    {
        public AjaxController()
        { }


        /// <summary>
        /// Redirect that remains on the same page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ToogleCaching()
        {
            SetCache();
            ActionResult result = Redirect(HttpContext.Request.UrlReferrer.AbsolutePath);
            return result;
        }


        [ChildActionOnly]
        public ActionResult RenderHeader()
        {
            IPhotosResult validatedPhotosResult = GetFacade().GetValidationResults();
            var model = new HeaderViewModel()
            {
                photosResult = validatedPhotosResult
            };
            return PartialView("partial/_Header", model);
        }

        [ChildActionOnly]
        public ActionResult RenderSignUp()
        {
            Models.CustomerModel cust = new Models.CustomerModel();
            return PartialView("partial/_SignUp", cust);
        }


        /// <summary>
        /// This is the Portfolio View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: Ajax
        [HttpGet, NoCache, LogActionFilter]
        public ActionResult GetPortfolioImages(string folder)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            IPhotoViewModel photoViewModel = GetFacade().GetImages(folder);
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;

            return PartialView("partial/_Image", photoViewModel);
        }

        /// <summary>
        /// This is the SlideShow that is the very first page to be displayed at start up.
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        // GET: Ajax
        [HttpGet, NoCache, LogActionFilter]
        public ActionResult GetSlideShowImages(string folder)
        {
            IPhotoViewModel photoViewModel = GetFacade().GetImages(folder);
            return PartialView("partial/_SlideShowImage", photoViewModel);
        }


        /// <summary>
        /// This is the Images that are requested from the drop-down selection on the Images view
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        // GET: Ajax
        [HttpGet, NoCache, LogActionFilter]
        public ActionResult SearchImages(string folder)
        {
            IPhotoViewModel photoViewModel = GetFacade().GetImages(folder);
            return PartialView("partial/_Image", photoViewModel);
        }
    }
}