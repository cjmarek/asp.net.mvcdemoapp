﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.Caching;


namespace ChrisMarekPhotography.Controllers
{
    public class ControllerBase : Controller
    {
        public class SessionKeys
        {
            public static readonly string ApplicationRoleKey = "__ApplicationRole";
            public static readonly string ApplicationCacheKey = "__ApplicationCache";
            public static readonly string PortfolioKey = "__Portfolio";
            public static readonly string ImagesKey = "__Images";
        }

        private IFacade _facadeDependency;


        public ControllerBase()
        {
        }

        internal void SetFacadeDependency(IFacade facadeDependency)
        {
            this._facadeDependency = facadeDependency;
        }

        protected IFacade GetFacade()
        {
            //return _facadeDependency ?? Factory.CreateFacade();
            if (_facadeDependency == null)
            {
                _facadeDependency = Factory.CreateFacade();
            }
            return _facadeDependency;



        }

        public virtual void SetCache()
        {
            object caching = Session[SessionKeys.ApplicationCacheKey];

            if (caching == null || (bool)caching == false)
            {
                Session[SessionKeys.ApplicationCacheKey] = true;
            }
            else
            {
                Session[SessionKeys.ApplicationCacheKey] = false;
                GetFacade().ClearCachedItems();
            }
        }
    }
}