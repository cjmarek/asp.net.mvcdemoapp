

The problems to solve with this web site are :

How can I have an easy to maintain photograph viewing system?

I want to not worry about the naming of photographs as I shoot new images that I want to include in the web site.
I want to be able to add new photos to the existing pool of photohgraphs by just coming up with a name that follows
an incrementing sequence. I also might want to re-use an earlier photo by just copying some new jpg photo over the old one.

      example of photo names in the images folder:          
      1.jpg
      2.jpg
      3.jpg 
      . 
      . 
      . 
      . 
      200.jpg
      201.jpg
      203.jpg
      etc etc etc

      So photo 1.jpg might be a photo of a blue bird, and photo 2.jpg and 3.jpg might be a photo of a elephant, and photo 200.jpg might be
      another photo of a blue bird. ect ect.

      I want to have folders that are named for the type of photo that is contained in the folder:

      examples.  A folder named bluebird that only contains blue bird data.
                 A folder named elephant that only contains elephant data.

      These folders will contain two text files per folder:  
         details.txt 
         indexes.txt

      These txt files will be comma separated lists.

      The details.txt file contains a 'comma separated list' of information relevant to the type of photo in the folder.
      Like only blue bird info in the bluebird folder.

      example details.txt file :    Sialia sialis,Irving Texas,birds,blue bird

      The indexes.txt file contains a 'comma separated list' of photo names (indexes) of only bluebird photos. 
      example photo names in indexes.txt file :   1,200

      The photos will display in the web site in the same order that the names in the indexes.txt file are listed.

      There will be a folder named slideshow that contains a indexes.txt file. No details.txt file is needed.
      There will be a folder named portfolio that contains a indexes.txt file. No details.txt file is needed.

      The indexes in the slideshow and portfolio folders can be any of the indexes (names) of photos in the entire pool of jpg photos.
      The indexes in the slideshow and portfolio folders will list the photos that shall appear in the website and the order they will appear.


      So, going forward, all I have to do to add new images to the system is:

      1) add the new jpg photo(s) to the images folder using the very next number in the sequence for the new jpg.

      2) add the jpg photo index number to the correct folder indexes.txt file for that subject folder 
          (if I have already got that folder out there.)

      3) or if the photo subject is a new subject. Add a new folder and add a details.txt and indexes.txt file to the new folder.

        It would be nice if this system is very robust and resistent to breaking if there are mistakes made in the configuring of the folders
        and text files.
        A basic validation of these folders should display any errors or problems on the web site for the site administrator to fix.

        RECAP:

        All actual photographs (jpg images) are in a single images folder.
        All photo data are in a photos folder that contains all the different photo subjects in their own folders.

























