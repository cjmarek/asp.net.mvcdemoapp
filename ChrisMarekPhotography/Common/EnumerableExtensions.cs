﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ChrisMarekPhotography.Common
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Returns the zero-based index of the first T element matching the supplied predicate.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"> the 'this' keyword causes the method to become a property of said IEnumerable.
        /// That is why this class is titled 'EnumerableExtensions'.
        /// In this way we extend the properties of the specified IEnumerable. Remember that predicate is the name
        /// we are using for our delegate. The delegate represents a pointer to anonymous method code
        /// that does a comparison and returns a boolean</param>
        /// <param name="predicate">variable name for the delegate that can be used for functions that accept a 'type' and 
        /// return a boolean</param>
        /// <returns></returns>
        public static int IndexOf<T>(this IEnumerable<T> items, Func<T, bool> predicate)
        {
            int matchIndex = -1;
            int index = 0;

            foreach (T item in items)
            {
                //this predicate does a comparison that resolves to a boolean
                if (predicate(item) == true)
                {
                    matchIndex = index;
                    break;
                }

                index++;
            }

            return matchIndex;
        }


        /// <summary>folderPhotoIndexes, fpi => fpi == req)
        /// NOTE: 'fpi == req', is an anonymous method that takes a string and resolves to a boolean.
        /// this method is pointed to by the delegate named 'predicate' that is passed to array.Search(predicate).
        /// var filteredIndexes = requiredIndexes.Where(req => Search(folderPhotoIndexes, fpi => fpi == req)).ToArray();
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool Search<T>(this IEnumerable<T> items, Func<T, bool> predicate)
        {
            bool result = false;

            foreach (T item in items)
            {
                // this predicate does a comparison that resolves to a boolean
                // see the example in the method documentation.
                // predicate(item) is actually a delegate to the anonymous method 'fpi == req'
                if (predicate(item) == true)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }
    }
}