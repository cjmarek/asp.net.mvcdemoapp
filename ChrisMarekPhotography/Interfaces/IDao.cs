﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChrisMarekPhotography
{
    public interface IDao
    {
        IEnumerable<string> GetAllPhotoFolders();
        string[] GetArray(string path);
        IEnumerable<string> GetPhotoFolderFiles(string folder);
    }
}
