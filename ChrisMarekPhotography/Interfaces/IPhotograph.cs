﻿namespace ChrisMarekPhotography
{
    public interface IPhotograph
    {
        string folder { get; set; }
        string index { get; set; }
    }
}