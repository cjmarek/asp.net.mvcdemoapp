﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChrisMarekPhotography
{
    public interface IPhotoObject
    {
        string[] Details { get; set; }
        string[] PhotoIndexes { get; set; }
    }
}