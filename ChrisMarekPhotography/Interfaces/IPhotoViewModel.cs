﻿using System.Collections.Generic;

namespace ChrisMarekPhotography
{
    public interface IPhotoViewModel
    {
        List<IPhotoInformation> PhotoInformation { get; set; }
    }
}