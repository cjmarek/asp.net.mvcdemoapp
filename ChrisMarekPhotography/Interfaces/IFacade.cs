﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChrisMarekPhotography
{
    public interface IFacade
    {
        string AddPhotoToDictionary(IPhotograph p, Dictionary<string, IPhotograph> photographDictionary);
        void CheckDataForErrors(IPhotosResult photosResult, string dataFile, string dataFolder);
        IEnumerable<string> CheckForPhotographDuplicates();
        void ClearCachedItems();
        List<IPhotograph> CreatePhotographList();
        List<string> GetAllPhotographIndexes();
        List<string> GetAllPhotoIndexes();
        IPhotosResult GetDuplicateFolderIndexEntries();
        List<IPhotoInformation> GetImageInfoForAssortedIndexes(string folder);
        List<IPhotoInformation> GetImageInfoForDropDownFolder(string folder);
        IPhotoViewModel GetImages(string dataFolder);
        List<string> GetKeys();
        string GetPath(string folder, string dataFile);
        string[] GetPhotoFolderData(string IndexPath);
        Dictionary<string, IPhotograph> GetPhotographDictionary();
        List<IPhotograph> GetPhotographList();
        List<IPhotograph> GetPhotoList();
        Dictionary<string, IPhotograph> GetPhotosDictionary();
        IPhotosResult GetValidatedFolders();
        IPhotosResult GetValidationResults();
        void InitializePhotoResultErrorMessages(IPhotosResult photosResult, string dataFile, string dataFolder, string failureDescription);
        void InitializeValidFolders(IPhotosResult aggregate, IPhotosResult validationResult, string folder);
        bool IsCachingOn();
        void LoadPhotographList(string folder, List<IPhotograph> photographs, string[] photoIndexes);
        List<IPhotoInformation> PullImagesFromFolder(string folder);
        T ReadThroughSlidingCache<T>(string key, Func<T> func, TimeSpan tsExpires);
        void SetDao(IDao dao);
        IPhotosResult ValidateAllFolders();
        IPhotosResult ValidateDupPhotoIndexInFolders();
        IPhotosResult ValidatePhotoFolderData(IPhotosResult photosResult, string dataFile, string dataFolder);
        IPhotosResult ValidatePhotoFolderFile(string IndexPath, string fileName, string folder);
        IPhotosResult ValidatePhotosFolder(string folder, string fileName);
    }
}