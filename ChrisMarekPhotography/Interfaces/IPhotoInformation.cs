﻿namespace ChrisMarekPhotography
{
    public interface IPhotoInformation
    {
        string ImageDescription { get; set; }
        string ImagePath { get; set; }
        int? Index { get; set; }
        string Location { get; set; }
        string SpecificType { get; set; }
        string Type { get; set; }
    }
}