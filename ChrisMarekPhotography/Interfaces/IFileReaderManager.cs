﻿namespace ChrisMarekPhotography
{
    public interface IFileReaderManager
    {
        string[] FileReader(string path);
    }
}