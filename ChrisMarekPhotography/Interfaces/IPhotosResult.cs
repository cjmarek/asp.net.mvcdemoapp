﻿using System.Collections.Generic;

namespace ChrisMarekPhotography
{
    public interface IPhotosResult
    {
        IEnumerable<string> ErrorMessages { get; }
        bool HasErrors { get; }
        List<IPhotoInformation> photoInformation { get; set; }
        string[] stringArray { get; set; }
        IEnumerable<string> ValidFolders { get; }

        void AddErrors(string failure, params object[] args);
        void AddValidFolder(string folder);
        void MergeFrom(IPhotosResult fromResult);
    }
}