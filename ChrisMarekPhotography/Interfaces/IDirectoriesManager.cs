﻿namespace ChrisMarekPhotography
{
    public interface IDirectoriesManager
    {
        string[] GetAllPhotoFolders();
    }
}