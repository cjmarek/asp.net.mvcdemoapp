﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChrisMarekPhotography.ExtractedDependencies;
using ChrisMarekPhotography.Models;

namespace ChrisMarekPhotography
{
    public static class Factory
    {
        public static IPhotosResult _photosResult = null;
        public static IPhotograph _photograph = null;
        public static IPhotoInformation _photoInformation = null;
        public static IPhotoObject _photoObject = null;
        public static IPhotoViewModel _photoViewModel = null;
        public static IDao _dao = null;
        public static IFacade _facade = null;
        public static IPathManager _pathManager = null;
        //these are all properties of the Dao
        public static IDirectoriesManager _directoriesManager = null;
        public static IFileReaderManager _fileReaderManager = null;
        public static IFolderFilesManager _folderFilesManager = null;

        public static IPathManager CreateServerPathManager()
        {
            //return _pathManager ?? new ServerPathManager();
            if (_pathManager == null)
            {
                _pathManager = new ServerPathManager();
            }
            return _pathManager;
        }

        public static void SetServerPathManagerDependency(IPathManager pathManager)
        {
            _pathManager = pathManager;
        }

        public static IDirectoriesManager CreateDirectoriesManager()
        {
            //return _directoriesManager ?? new DirectoriesManager();
            if (_directoriesManager == null)
            {
                _directoriesManager = new DirectoriesManager();
            }
            return _directoriesManager;
        }

        public static void SetDirectoriesManagerDependency(IDirectoriesManager directoriesManager)
        {
            _directoriesManager = directoriesManager;
        }


        public static IFolderFilesManager CreateFolderFilesManager()
        {
            //return _folderFilesManager ?? new FolderFilesManager();
            if (_folderFilesManager == null)
            {
                _folderFilesManager = new FolderFilesManager();
            }
            return _folderFilesManager;
        }

        public static void SetFolderFilesManagerDependency(IFolderFilesManager folderFilesManager)
        {
            _folderFilesManager = folderFilesManager;
        }

        public static IFileReaderManager CreateFileReaderManager()
        {
            //return _fileReaderManager ?? new FileReaderManager();
            if (_fileReaderManager == null)
            {
                _fileReaderManager = new FileReaderManager();
            }
            return _fileReaderManager;
        }

        public static void SetFileReaderManagerDependency(IFileReaderManager fileReaderManager)
        {
            _fileReaderManager = fileReaderManager;
        }

        public static IPhotosResult CreatePhotosResult()
        {
            return _photosResult ?? new PhotosResult();
            //if (_photosResult == null)
            //{
            //    _photosResult = new PhotosResult();
            //}
            //return _photosResult;
        }

        public static void SetPhotosResultDependency(IPhotosResult photosResult)
        {
            _photosResult = photosResult;
        }

        public static IPhotograph CreatePhotograph()
        {
            return _photograph ?? new Photograph();
            //if (_photograph == null)
            //{
            //    _photograph = new Photograph();
            //}
            //return _photograph;
        }

        public static void SetPhotographDependency(IPhotograph photograph)
        {
            _photograph = photograph;
        }

        public static IPhotoInformation CreatePhotoInformation(string s1, string s2, string s3, string s4, int index)
        {
            return _photoInformation ?? new PhotoInformation(s1, s2, s3, s4, index);
            //if (_photoInformation == null)
            //{
            //    _photoInformation = new PhotoInformation(s1, s2, s3, s4, index);
            //}
            //return _photoInformation;
        }

        public static void SetPhotoInformationDependency(IPhotoInformation photoInformation)
        {
            _photoInformation = photoInformation;
        }

        public static IPhotoObject CreatePhotoObject(string[] s1, string[] s2)
        {
            return _photoObject ?? new PhotoObject(s1, s2);
            //if (_photoObject == null)
            //{
            //    _photoObject = new PhotoObject(s1, s2);
            //}
            //return _photoObject;
        }

        public static void SetPhotoObjectDependency(IPhotoObject photoObject)
        {
            _photoObject = photoObject;
        }

        public static IPhotoViewModel CreatePhotoViewModel(List<IPhotoInformation> photoInformation)
        {
            return _photoViewModel ?? new PhotoViewModel(photoInformation);
            //if (_photoViewModel == null)
            //{
            //    _photoViewModel = new PhotoViewModel(photoInformation);
            //}
            //return _photoViewModel;
        }

        public static void SetPhotoViewModelDependency(IPhotoViewModel photoViewModel)
        {
            _photoViewModel = photoViewModel;
        }

        public static IDao CreateDao()
        {
            //return _dao ?? new Dao();
            if (_dao == null)
            {
                _dao = new Dao();
            }
            return _dao;
        }

        public static void SetDaoDependency(IDao dao)
        {
            _dao = dao;
        }

        public static IFacade CreateFacade()
        {
            //return _facade ?? new Facade();
            if (_facade == null)
            {
                _facade = new Facade();
            }
            return _facade;
        }
        public static void SetFacadeDependency(IFacade facade)
        {
            _facade = facade;
        }
    }
}